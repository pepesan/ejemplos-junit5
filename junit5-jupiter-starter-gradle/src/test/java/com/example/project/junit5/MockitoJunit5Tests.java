package com.example.project.junit5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class MockitoJunit5Tests {
    @Mock
    UserRepository userRepository;

    @BeforeEach
    void init() {
        when(this.userRepository.getUserMinAge()).thenReturn(4);
    }

    @Test
    void miTestParametro(@Mock UserRepository userRepository){
        when(userRepository.getUserMinAge()).thenReturn(4);
        assertEquals(4,this.userRepository.getUserMinAge());
        assertEquals(4,userRepository.getUserMinAge());
    }


}
